const express = require('express');
const bodyParser = require('body-parser');
const uuid = require('uuid/v4');

const app = express();

const DUMMY_DATA = [
  {
    "userName": "rajat",
    "password": "1234",
    "token": "sakjkahsjhasjhks"
  }
]; // not a database, just some in-memory storage for now

app.use(bodyParser.json());

// CORS Headers => Required for cross-origin/ cross-server communication
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, DELETE, OPTIONS'
  );
  next();
});

app.post('/login', (req, res, next) => {
  const { userName, password } = req.body;

  if (!userName || userName.trim().length === 0 || !password || password.trim().length === 0) {
    return res.status(422).json({
      message: 'Invalid input, please enter a valid userName and password.'
    });
  }

  const newData = DUMMY_DATA.filter(d => {
    if (d.userName == userName && d.password == password) {
      return d;
    }
  });
  if (newData.length)
    res.status(200).json(newData[0]);
  else
    res.status(200).json({ 'userName': '', 'password': '', 'token': '' });
});

app.post('/signup', (req, res, next) => {
  const { userName, password, age, gender } = req.body;

  if (!userName || userName.trim().length === 0 || !password || password.trim().length === 0) {
    return res.status(422).json({
      message: 'Invalid input, please enter a valid userName and password.'
    });
  }

  const createdUser = {
    token: uuid(),
    userName,
    password,
    age,
    gender,
  };

  DUMMY_DATA.push(createdUser);

  res
    .status(201)
    .json({ message: 'Created new User.', data: createdUser });
});

app.listen(5000); // start Node + Express server on port 5000
