
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

// import withHeaderWrapper from '../Component/Comman/Header/withHeaderWrapper';

import authHOC from "../util/authHOC";
import PageNotFound from '../Container/Pagenotfound';
import Home from '../Container/PhotoList';
import Login from '../Container/Login';
import SignUp from '../Container/SignUp';

const routes = (props) => {
    return (
        <Router>
            <Switch>
                <Route
                    exact
                    path="/"
                    component={Login} />
                <Route
                    exact
                    path="/signup"
                    component={SignUp} />
                <Route
                    exact
                    path="/home"
                    component={authHOC(Home)} />
                <Route
                    path=""
                    component={PageNotFound} />
            </Switch>
        </Router>
    );
}

export default routes;