import React from 'react'

import './style.css';
const PhotoCard = (props) => {
    const { avtar, dis } = props.data;
    return (
        <div className="card">
            <img src={require(`../../../Asset/images/${avtar}`)} alt="Avatar" style={{ width: "100%" }} />
            <div className="container">
                <p>{dis}</p>
            </div>
        </div>
    )
}

export default PhotoCard;