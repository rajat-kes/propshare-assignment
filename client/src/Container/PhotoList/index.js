import React, { Component } from 'react'

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as action from './Actions';
import { logout } from '../../Container/Login/Actions';

import { withRouter } from "react-router";

import './style.css';
import PhotoCard from './PhotoCard';
export class PhotoList extends Component {

    changeURL = (url) => {
        this.props.history.push(url);
    }

    render() {
        const { list } = this.props.photosList;
        const { logout } = this.props;
        return (
            <React.Fragment>
                <div className=''>
                    <button
                        className='btn btn--white input-box login-btn'
                        type="button"
                        onClick={logout}
                    >
                        {'logout'}
                    </button>
                </div>
                <div className='header'>
                    <div className='photos-card'>
                        {list.map(d => {
                            return (
                                <PhotoCard key={Math.random()} data={d} />
                            )
                        })}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        photosList: state.photosList,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...action,
        logout,
    }, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PhotoList))
