import React, { Component } from 'react';

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as action from '../Login/Actions';

import { withRouter } from "react-router";

import SignUpForm from './Form';

import './style.css';

export class Login extends Component {
    handleChangeURL = (url) => {
        if (url) {
            this.props.history.push(url);
        }
    }

    handleCheckOutAuthUser = () => {
        const { token } = this.props.loginState.userData;
        const { handleChangeURL } = this;
        if (token) {
            handleChangeURL('/home');
        }
    }
    componentDidMount = () => {
        this.handleCheckOutAuthUser();
    }

    componentDidUpdate = () => {
        this.handleCheckOutAuthUser();
    }

    render() {
        const { handleChangeURL } = this;
        const { SignUp } = this.props;
        return (
            <div className='header-signup'>
                <div className="header-signup__text-box">
                    <SignUpForm
                        handleSignUp={SignUp}
                    />
                    <span onClick={() => handleChangeURL('/')}>if already account</span>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loginState: state.login,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...action
    }, dispatch)
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))
