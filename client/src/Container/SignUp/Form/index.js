import React, { useState } from 'react'

import './style.css';

const Form = (props) => {
    const { handleSignUp } = props
    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [pass, setPass] = useState('');
    const [gender, setGender] = useState('');
    const [age, setAge] = useState('');
    return (
        <div>
            <h1 className='heading-text'>{'What is your name?'}</h1>
            <input
                className='input-box input-box__border '
                type="text"
                id="FastName"
                name="FastName"
                placeholder='Fast Name *'
                value={fname}
                onChange={(event) => setFname(event.target.value)}
            />
            <input
                className='input-box input-box__border '
                type="text"
                id="Last Name"
                name="Last Name"
                placeholder='Last Name'
                value={lname}
                onChange={(event) => setLname(event.target.value)}
            />
            <input
                className='input-box input-box__border '
                type="password"
                id="password"
                name="password"
                placeholder='password *'
                value={pass}
                onChange={(event) => setPass(event.target.value)}
            />
            <h1 className='heading-text'>{'And your gender?'}</h1>
            <button
                className={`btn btn-gender ${gender === 'male' ? 'btn-selected' : 'btn--white'}`}
                type="button"
                onClick={() => setGender('male')}
            >
                {'Male'}
            </button>
            <button
                className={`btn btn-gender ${gender === 'female' ? 'btn-selected' : 'btn--white'}`}
                type="button"
                onClick={() => setGender('female')}
            >
                {'Female'}
            </button>
            <h1 className='heading-text'>{'And your age?'}</h1>
            <input
                className='input-box input-box__border '
                type="date"
                id="Last Name"
                name="Last Name"
                placeholder='Last Name'
                value={age}
                onChange={(event) => setAge(event.target.value)}
            />
            <button
                className='btn btn--white input-box'
                type="button"
                onClick={() => {
                    if (fname && pass) {
                        handleSignUp({ fname, lname, pass, gender, age });
                    }
                }}
            >
                {'Sign up'}
            </button>
        </div>
    )
}

export default Form;