import * as types from './ActionTypes';

const initialState = {
    userData: {}
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_USERDATA:
            return {
                ...state,
                userData: action.payload,
            }
        case types.CLEAR_USERDATA:
            return {
                ...state,
                userData: initialState.userData,
            }
        default:
            return state
    }
}
