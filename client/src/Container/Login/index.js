import React, { Component } from 'react';

import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as action from './Actions';

import { withRouter } from "react-router";

import LoginForm from './Form';

import './style.css';

export class Login extends Component {
    handleChangeURL = (url) => {
        if (url) {
            this.props.history.push(url);
        }
    }

    handleCheckOutAuthUser = () => {
        const { token } = this.props.loginState.userData;
        const { handleChangeURL } = this;
        if (token) {
            handleChangeURL('/home');
        }
    }
    componentDidMount = () => {
        this.handleCheckOutAuthUser();
    }

    componentDidUpdate = () => {
        this.handleCheckOutAuthUser();
    }

    render() {
        const { handleChangeURL } = this;
        const { Login } = this.props;
        return (
            <div className='header login'>
                <div className="header__logo">
                    <h1 className="heading-primary">
                        <span className="heading-primary--main">PWA</span>
                        <span className="heading-primary--sub">CatchPhrase</span>
                    </h1>
                </div>
                <span><b>Note:</b> test username: <b>rajat</b> pass: <b>1234</b></span>
                <div className="header__text-box">
                    <LoginForm
                        handleChangeURL={handleChangeURL}
                        handleLogin={Login}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loginState: state.login,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...action
    }, dispatch)
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))
