import React, { useState } from 'react'

import './style.css';

const Form = (props) => {
    const { handleChangeURL, handleLogin } = props
    const [name, setName] = useState('');
    const [pass, setPass] = useState('');
    return (
        <div>
            <input
                className='input-box'
                type="text"
                id="UserName"
                name="UserName"
                placeholder='Username'
                value={name}
                onChange={(event) => setName(event.target.value)}
            />
            <input
                className='input-box'
                type="password"
                id="Password"
                name="Password"
                placeholder='Password'
                value={pass}
                onChange={(event) => setPass(event.target.value)}
            />
            <button
                className='btn btn--white login-btn'
                type="button"
                onClick={() => {if(name.trim() && pass.trim()) handleLogin({ name, pass })}}
            >
                {'Log in'}
            </button>
            <button
                className='btn btn--white login-btn'
                type="button"
                onClick={() => handleChangeURL('/signup')}>
                {'Sign up'}
            </button>
        </div>
    )
}

export default Form;