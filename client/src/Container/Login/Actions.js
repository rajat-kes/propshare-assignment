import * as types from './ActionTypes';

import { baseUrl } from '../../util/APIData';
import axios from 'axios';

const clearToken = () => {
    return (dispatch, getState) => {
        return dispatch({ type: types.CLEAR_USERDATA, payload: {} })
    }
}

const setUserData = (data) => {
    return (dispatch, getState) => {
        return dispatch({ type: types.SET_USERDATA, payload: data })
    }
}

export const logout = () => {
    return (dispatch, getState) => {
        dispatch(clearToken());
    }
}

export const Login = (data) => {
    const { name, pass } = data;
    return (dispatch, getState) => {
        dispatch(clearToken())
        const url = `${baseUrl}/login`;
        const data = {
            "userName": name,
            "password": pass
        }
        axios.post(url, data)
            .then((res) => {
                // console.log(res.data);
                return dispatch(setUserData(res.data));
            })
            .catch((error) => {
                console.log(error);
            });
    }
}

export const SignUp = (data) => {
    const { fname, lname, pass, gender, age } = data;
    return (dispatch, getState) => {
        dispatch(clearToken())
        const url = `${baseUrl}/signup`;
        const data = {
            "userName": fname + ' ' + lname,
            "password": pass,
            gender,
            age,
        }
        axios.post(url, data)
            .then((res) => {
                return dispatch(setUserData(res.data.data));
            })
            .catch((error) => {
                console.log(error);
            });
    }
}