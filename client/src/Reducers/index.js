import storage from 'redux-persist/lib/storage'
import { persistCombineReducers } from 'redux-persist'

import login from '../Container/Login/reducer';
import photosList from '../Container/PhotoList/reducer';

const reducers = {
    'login': login,
    'photosList': photosList,
}

export const persistConfig = {
    key: 'ASSIGNMENT.0.0.1',
    storage,
    blacklist: [
        'photosList',
    ]
}

const appReducer = persistCombineReducers(persistConfig, reducers)

const rootReducer = (state, action) => {
    return appReducer(state, action)
};

export default rootReducer;